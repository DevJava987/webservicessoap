package com.aam.ws.impl;

import java.util.List;

import com.aam.ws.api.BookService;
import com.aam.ws.model.Author;
import com.aam.ws.model.Book;
import com.aam.ws.model.BookNotFoundException;
import com.aam.ws.model.BookType;


public class BookServiceImpl  implements BookService {

	private Long counter = 0L;
	
	@Override
	public Book getBook(Long id) throws BookNotFoundException {
		Book book = new Book();
		book.setId(66L);
		book.setTitle("title");
		book.setType(BookType.HEROIC_FANTASY);
		book.setYear(2019);
		return book;
	}

	@Override
	public List<Book> getBooksByTitle(String title) {
		return null;
	}

	@Override
	public List<Author> getAuthorsFromBook(String bookTitle) throws BookNotFoundException {
		return null;
	}

	@Override
	public Long createBook(String title, BookType type, int year, Long... authorsId) {
		counter++;
		return counter;
	}

}