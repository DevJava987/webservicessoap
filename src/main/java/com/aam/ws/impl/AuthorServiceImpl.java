package com.aam.ws.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.jws.WebMethod;
import javax.jws.WebService;

import com.aam.ws.api.AuthorService;
import com.aam.ws.model.Author;
import com.aam.ws.model.AuthorNotFoundException;
import com.aam.ws.model.Book;


@WebService(name="AuthorService", endpointInterface="com.aam.ws.api.AuthorService",
			serviceName="AuthorService")
public class AuthorServiceImpl implements AuthorService {
	
	private Long counter = 0L;

	@Override
	@WebMethod(action="getBooksFromAuthor")
	public List<Book> getBooksFromAuthor(String authorFirstName, String authorLastName) throws AuthorNotFoundException {
		List<Book> books = new ArrayList<>();
		books.add(new Book());
		return books;
	}

	@Override
	public Author getAuthor(Long id) throws AuthorNotFoundException {
		return null;
	}

	@Override
	public List<Author> getAuthorsByName(String firstName, String lastName) throws AuthorNotFoundException {
		return null;
	}

	@Override
	public Long createAuthor(String firstName, String lastName, String nationality, Date dateOfBirth, Date dateOfDeath) {
		counter++;
		return counter;
	}

}