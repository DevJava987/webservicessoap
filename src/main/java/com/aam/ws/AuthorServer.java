
import org.apache.cxf.jaxws.JaxWsServerFactoryBean;

import com.aam.ws.impl.AuthorServiceImpl;

public class AuthorServer {

	/**
	 * This will deploy a CXF WS based on annotations specified in the interface AuthorService
	 * and the class AuthoerServiceImpl. the config od this service is fully done with annotation, no spring!
	 * To see an example of WS configured with Spring have a look at BookService.
	 */
	public static void main (String[] args) throws InterruptedException {
		
		AuthorServiceImpl authorService = new AuthorServiceImpl();
		JaxWsServerFactoryBean svrFactory = new JaxWsServerFactoryBean(); 
		svrFactory.setServiceClass(AuthorServiceImpl.class); 
		svrFactory.setAddress("http://localhost:9000/AuthorService"); 
		svrFactory.setServiceBean(authorService); 
		svrFactory.create();
		
		Thread.sleep(5 * 60 * 1000);
	}
	
}
