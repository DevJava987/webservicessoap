package com.aam.ws.api;

import java.util.Date;
import java.util.List;

import javax.jws.WebService;

import com.aam.ws.model.Author;
import com.aam.ws.model.AuthorNotFoundException;
import com.aam.ws.model.Book;


@WebService
public interface AuthorService {

	List<Book> getBooksFromAuthor(String authorFirstName, String authorLastName) throws AuthorNotFoundException;

    Author getAuthor(Long id) throws AuthorNotFoundException;

    List<Author> getAuthorsByName(String firstName, String lastName) throws AuthorNotFoundException;

    Long createAuthor(String firstName, String lastName, String nationality, Date dateOfBirth, Date dateOfDeath);
	
}
