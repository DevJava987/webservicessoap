package com.aam.ws.api;

import java.util.List;

import javax.jws.WebParam;
import javax.jws.WebService;

import com.aam.ws.model.Author;
import com.aam.ws.model.Book;
import com.aam.ws.model.BookNotFoundException;
import com.aam.ws.model.BookType;

@WebService(name="BookService", serviceName="BookService")
public interface BookService {

	Book getBook(@WebParam(name="id") Long id) throws BookNotFoundException;

    List<Book> getBooksByTitle(@WebParam(name="title") String title);

    List<Author> getAuthorsFromBook(@WebParam(name="bookTitle") String bookTitle) throws BookNotFoundException;

    Long createBook(String title, BookType type, int year, Long... authorsId);
	
}
