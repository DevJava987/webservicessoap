package com.aam.ws.model;

public enum BookType {
	
	SCIENCE_FICTION, POLICIER, ROMAN_HISTORIQUE, HEROIC_FANTASY
	
}
