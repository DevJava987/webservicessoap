package com.aam.ws.model;

import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

@XmlRootElement(name="Book")
@XmlType(name="Book")
@XmlAccessorType(XmlAccessType.FIELD)
public class Book {

	@XmlElement(name = "id")
	private Long id;
	
	@XmlElement(name="title")
	private String title;
	
	@XmlElement(name="type")
    private BookType type;
	
	@XmlElement(name="year")
    private int year;

    public Long getId() {
		return id;
	}
	
	public void setId(Long id) {
		this.id = id;
	}
	
	public String getTitle() {
		return title;
	}
	
	public void setTitle(String title) {
		this.title = title;
	}
	
	public BookType getType() {
		return type;
	}
	
	public void setType(BookType type) {
		this.type = type;
	}
	
	
	public int getYear() {
		return year;
	}
	public void setYear(int year) {
		this.year = year;
	}    
}