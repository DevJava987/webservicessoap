package com.aam.ws;

public enum BookType {
	
	SCIENCE_FICTION, POLICIER, ROMAN_HISTORIQUE, HEROIC_FANTASY
	
}
