import java.util.Date;

import com.aam.ws.api.AuthorService;
import com.aam.ws.api.BookService;
import com.aam.ws.model.Book;
import com.aam.ws.model.BookType;
import org.apache.cxf.frontend.ClientProxyFactoryBean;


public class Client {


  private Client() {
  }

  public static void main(String[] args) throws Exception {

	  ClientProxyFactoryBean factory = new ClientProxyFactoryBean();
	  factory.setServiceClass(AuthorService.class);
	  factory.setAddress("http://localhost:9000/AuthorService");
	  AuthorService ws = (AuthorService) factory.create();
	  Long res1 = ws.createAuthor("toto", "tata", "french", new Date(), new Date());
	  System.out.println("OK "+res1);
	  Long res2 = ws.createAuthor("toto", "tata", "french", new Date(), new Date());
	  System.out.println("OK "+res2);

	  
  }
  
//  public static void main(String[] args) throws Exception {
//
//	  ClientProxyFactoryBean factory = new ClientProxyFactoryBean();
//	  factory.setServiceClass(BookService.class);
//	  factory.setAddress("http://localhost:8080/WebServiceJavaFirst/services/book");
//	  BookService ws = (BookService) factory.create();
//	  Long result1 = ws.createBook("title", BookType.HEROIC_FANTASY, 2019, 1L);
//	  System.out.println("OK "+ result1);
//
//	  Long result2 = ws.createBook("title", BookType.HEROIC_FANTASY, 2019, 1L);
//	  System.out.println("OK "+ result2);
//	  
////	  try {
////	  Book book = ws.getBook(0L);
////	  } catch (Exception ex) {
////		  System.out.println("ERROR");
////	  }
////	  System.out.println("OK");
//  }

  
	
}
